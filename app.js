const yargs = require("yargs");
const { simpanContact, listContact, detailContact, deleteContact } = require("./contacts");

yargs.command({
  command: "add",
  describe: "Menambahkan contact baru",
  builder: {
    nama: {
      describe: 'Nama lengkap',
      demandOption:true,
      type:'string',
    },
    email: {
      describe: 'Email',
      demandOption:false,
      type:'string',
    },
    noHP: {
      describe: 'Nomor Handphone',
      demandOption:true,
      type:'string',
    },
  },
  handler(argv){
    simpanContact(argv.nama, argv.email, argv.noHP)
  },
}).demandCommand();

//menampilkan data semua contact
yargs.command({
  command: "list",
  describe: "Menampilkan semua contact",
  handler(argv){
    listContact(argv.nama)
  },
});

//menampilkan detail data berdasarkan nama
yargs.command({
  command: "detail",
  describe: "Menampilkan detail contact",
  builder: {
    nama: {
      describe: 'Nama lengkap',
      demandOption:true,
      type:'string',
    },
  },
  handler(argv){
    detailContact(argv.nama)
  },
});

//menghapus contact berdasarkan nama 
yargs.command({
  command: "delete",
  describe: "Delete contact",
  builder: {
    nama: {
      describe: 'Nama lengkap',
      demandOption:true,
      type:'string',
    },
  },
  handler(argv){
    deleteContact(argv.nama)
  },
});

yargs.parse();
// const contacts = require('./contacts');

// const main = async () => {
//   const nama = await contacts.tulisPertanyaan("Masukkan nama anda : ");
//   const email = await contacts.tulisPertanyaan("Masukkan email anda : ");
//   const noHP = await contacts.tulisPertanyaan("Masukkan No HP anda : ");

//   contacts.simpanContact(nama, email, noHP);
// };

// main();
